namespace DemoCicdAzureWebApp.Models
{
    // This class is a model for the error view.
    public class ErrorViewModel
    {
        public string? RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
